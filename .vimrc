filetype plugin indent on
set grepprg=grep\ -nH\ $*
let g:tex_flavor = "latex"

set runtimepath=~/.vim,$VIM/vimfiles,$VIMRUNTIME,$VIM/vimfiles/after,~/.vim/after

set background=dark

filetype indent on
syntax enable

set backspace=indent,eol,start

set backspace=2
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab

set number
set relativenumber

set cursorline


set wildmenu

set lazyredraw

set showmatch

set incsearch
set hlsearch

if has ('persistent_undo')
    set undofile
    set undodir=$HOME/.vim/undo
endif


nnoremap j gj
nnoremap k gk
nnoremap gV `[v`]

call plug#begin('~/.vim/plugged')
Plug 'junegunn/vim-easy-align'
Plug 'scrooloose/nerdtree'
Plug 'vim-syntastic/syntastic'
Plug 'vim-airline/vim-airline'
Plug 'suy/vim-context-commentstring'
Plug 'tpope/vim-surround'
Plug 'godlygeek/tabular'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-commentary'
Plug 'FredKSchott/CoVim'
Plug 'crusoexia/vim-monokai'

""" NERDtree settings

" Toggle nerdtree using Ctrl+k
map <C-k> <plug>NERDTreeTabsToggle<CR>
" Don't open NERDTree when new tab opens (cos we're using buffers instead of
" tabs now)
let g:nerdtree_tabs_open_on_new_tab = 0
" Only open nerdtree if we opened vim in a directory
let g:nerdtree_tabs_open_on_gui_startup = 2
let g:nerdtree_tabs_open_on_console_startup = 2

" Hide certain file types in NERDtree
let NERDTreeIgnore = ['\.pyc$', '\.class$']

""" Setup CtrlP so that it doesn't open files in NERDtree buffer
function! CtrlPCommand()
    let c = 0
    let wincount = winnr('$')
    " Don't open it here if current buffer is not writable (e.g. NERDTree)
    while !empty(getbufvar(+expand("<abuf>"), "&buftype")) && c < wincount
        exec 'wincmd w'
        let c = c + 1
    endwhile
    exec 'CtrlP'
endfunction

let g:ctrlp_cmd = 'call CtrlPCommand()'

" make it show hidden files
let g:ctrlp_show_hidden = 1


""" Remap the leader key (\) to ö for ease on Swedish keyboard
let mapleader = "ö"